# Do Zero à Nuvem: API Restful com NodeJS, Restify e Mongoose, MongoDB

Dicas de Plug-ins, Módulos:

Google Chrome: **JSONView**

Lembrando:

```
npm i install yargs --save
```
colocará o pacote yargs no **dependences** no package.json;

Instale os Types dos pacotes instalados para utilizar o **autocomplete** do 
```
npm i @types/yargs -D (ou --save)
```

## Objetivo

Crie uma API com NodeJS, Restify, Mongoose, MongoDB e use o Gitlab para Automatizar o Deploy em Ambiente na Digital Ocean

--

Neste curso vamos criar uma API Restful em NodeJS. Para isso, vamos usar uma biblioteca bem poderosa para criação de APIs que se chama Restify.

A partir daí, a gente vai crescer essa aplicação com MongoDB e Mongoose, vamos aprender a validar os dados, fazer tratamento de erros, deixar a nossa API navegável e também adicionar segurança. Claro que não poderiam faltar os fundamentos, então também vou falar do protocolo HTTP e sobre os padrões de uma API Restful. 

Vamos testar a API utilizando o Jest, uma biblioteca de testes do facebook.

Vou mostrar todo o passo-a-passo para se criar um ambiente de produção na Digital Ocean com domínio registrado e certificado SSL gratuito com Let's Encrypt. Só pra deixar o cenário mais interessante, vamos ver como podemos automatizar o deploy da aplicação sempre que for feita uma mudança no código. 

E tudo isso vamos fazer com Typescript. Uma linguagem que extende o Javascript com tipos estáticos e que permite a gente detectar problemas mais rapidamente no desenvolvimento.

Aguardo você no curso.

Quais são os requisitos?

Importante conhecer Javascript (ECMAScript 2015) - Classes, Arrow Functions e Módulos
Conhecer Promises
Desejável ter noções de Typescript
O que serei capaz de fazer depois deste curso?

Criar um backend com Typescript e NodeJS
Usar o Restify para criar uma API REST (rotas, gerenciamento de erros, múltiplas versões, logging, plugins)
Utilizar o Mongoose para manipulação de documentos
Adicionar hypermedia na API
Criar suítes de testes com Jest e Supertest em Typescript
Implementar autenticação e autorização com tokens (JWT) e perfis
Gerenciar múltiplos processos em cluster com PM2
Configurar um ambiente completo na Digital Ocean (com certificado SSL gratuito do Lets Encrypt)
Publicar o código fonte no Gitlab
Configurar a ferramenta de integração contínua do Gitlab para testes automatizados
Configurar o Gitlab para fazer deploy automatizado no ambiente de produção (Digital Ocean)
Entender o que é CORS e como podemos nos livrar dele
Conhecer o protocolo HTTP e seus métodos
Saber o que é uma API Restful
Qual é o público-alvo?

Desenvolvedores que desejam conhecer os fundamentos de uma API REST
Desenvolvedores que querem implementar um backend com Typescript
Desenvolvedores que querem implementar um backend com NodeJS e Restify
Desenvolvedores que querem incorporar Mongoose e MongoDB na sua aplicação


## Seção 2

### O que é o Node.JS
### Instalação do Node.JS no Windows
### Instalação do Node.JS no Linux/Mac OS
### Modo REPL - Repeat, Evaluate, Print Loop
### Primeiro Script

```javascript
console.log('n-fatorial')

const fatorial  = (num)=>{
    if(num === 0) {
      return 1
    }

    return num * fatorial (num - 1)
}

console.log(`O fatorial de 5 é igual a ${fatorial(5)}`)
```

Para executar faça: 
```
node script.js
```

### Objetos Global e Process

```javascript
console.log('n-fatorial')

/*console.log(`Executando o script a partir do diretório ${process.cwd()}`)

process.on('exit', ()=>{
  console.log('script está prestes a terminar')
})*/

const fatorial  = (num)=>{
    if(num === 0) {
      return 1
    }

    return num * fatorial (num - 1)
}

const num = parseInt(process.argv[2])

console.log(`O fatorial de ${num} é igual a ${fatorial(num)}`)
```


No Node.JS (server-side) nós não temos o objeto **window** no servidor. Um objeto "análogo" ao **window** é o objeto **global** para ver todos os módulos do Node.js disponíveis no objeto **global** digite **node** e pressione **TAB** que ele mostrará todas as funções que poderemos utilizar.

O objeto PRINCIPAL é o objeto **process** herda do **Event Emiter**. Ele faz uma ponte entre o **AMBIENTE** que estamos executando e o **script**. Através dele podemos saber qual é o diretório do **script**, podemos **ouvir** **eventos** (Ex: ficar ouvindo quando o script irá terminar, escutar eventos do sistema operacional).

Com o objeto process podemos saber quais foram os **parâmetros** que foram passados para o script.

- Exemplo para saber em qual diretório o script foi executado (process.cwd()):

```javascript
console.log('n-fatorial')
/* Com process você tem acesso ao diretório de execução do script */
console.log(`Executando o script a partir do diretório ${process.cwd()}`)

/* Com process você pod OUVIR quando o Script irá SAIR (exit) do process.on() */
process.on('exit', ()=>{
  console.log('script está prestes a terminar')
})

const fatorial  = (num)=>{
    if(num === 0) {
      return 1
    }

    return num * fatorial (num - 1)
}

/* Com process você tem acesso a todos os argumentos que foram utilados para 
iniciar o script */
const num = parseInt(process.argv[2])
console.log(`O fatorial de ${num} é igual a ${fatorial(num)}`)
```

Executando o script: 
```
node meu-script.js 10
```
Será calculado o fatorial de **10** pois ele pegou o 10 com **process.argv[2]**

**Útil:** ouvir eventos de saída, para fazer uma espécie de **limpeza** (fechar banco conexões de BD), para isso vamos utilizar o **método process.on('exit')**. Exemplo:

### Usando Módulos Built-in (core do Node.JS)

São módulos que fazem parte do CORE do Node.JS. Todo arquivo de uma aplicação que nós acrescentamos acaba fazendo parte de um todo. Corre o risco de choques de nomes. Em Node.JS isso NÃO ACONTECE, cada arquivo JS de uma App é considerado um MÓDULO, e cada MÓDULO tem um Scopo isolado. Quando queremos utilizar uma função declarada em outro módulo. Como fazemos, temos que usar o COMMOMJS para EXPORTAR esta função.

Primeiro Passo, como importar um Módulo CORE para usar suas funções?

Script: save-file.js da aula 08

Utilizando o Módulo Core **fs** do Node:

```javascript

/** Importando o Módulo Core.. se não for Core ele buscará o módulo personalizado */
const fs = require('fs')

/** Lembrando que estamos utilizando o Módulo process.argv para obter os argumentos passados na execução */

/** 3 parêmtros: nome do arquivo, conteúdo e um listener (sucesso ou erro) */
fs.writeFile(process.argv[2], process.argv[3], (error)=>{
    if(error) throw error
    console.log(`Arquivo ${process.argv[2]} foi salvo com sucesso.`)
})
```


### Usando Módulos Personalizados

Script main.js que importa o módulo personalizado **fatorial** 

```javascript

const fatorial = require('./fatorial')

console.log('n-fatorial')

/*console.log(`Executando o script a partir do diretório ${process.cwd()}`)

process.on('exit', ()=>{
  console.log('script está prestes a terminar')
})*/

const num = parseInt(process.argv[2])

console.log(`O fatorial de ${num} é igual a ${fatorial(num)}`)
```

Lembrando que cada arquivo da APP é considerado um módulo, e cada módulo tem seu Scopo, e para **exportar** alguma função ou variável utilizamos o **commonjs** com a sintaxe:

```javascript

/** Método que será exportado */
const fatorial  = (num)=>{
    if(num === 0) {
      return 1
    }

    return num * fatorial (num - 1)
}

/** Várias Maneiras de Exportar */

//exports.fatorial = fatorial

//exports = module.exports

/** Padrão que utilizaremos */
module.exports = fatorial

/*module.exports = {
  fatorial,
  funcao: funcao2
}*/
```

#### Importando o Módulo Yargs

```
npm install yargs --save
```

Usando o yargs e removendo o process.argv.

Importando o objeto **yargs**:

```javascript
const argv = require('yargs')
```

Importando uma função específica chamada demandOption() onde defino
o nome do parâmetro de entrada, (ex: parâmetro num):

```javascript
const argv = require('yargs').demandOption('num').argv
```

Usando:

```javascript
const num = argv.num
```

Modo de rodar:

```javascript
node main.js --num=6
```

### Convertendo os Exemplos em Typescript

Verifique se está instalado globalmente:

```
npm i typescript -g
```

Iniciando o Typescript:

```
tsc --init
```

Limpe o conteúdo de **tsconfig.json** e deixe somente:
```json
{
  "compilerOptions": {
    "outDir": "dist",
    "target": "es2015",
    "module": "commonjs",
    "sourceMap": true
  }
}
```

#### Instalando as Definições das Bibliotecas - Projeto DefineTyped

Instale os Types dos pacotes instalados para utilizar o **autocomplete** do 
```
npm i @types/yargs -D (ou --save)
```

Compilando o Typescript (watch):

```
tsc -w
```
Alterando os scripts para Typescript

**main.ts**

**observe a nova forma de importar pacotes:**

```typescript
import {fatorial} from './fatorial'
import * as yargs from 'yargs'

console.log('=== n-fatorial ===')

const argv = yargs.demandOption('num').argv

const num = argv.num

console.log(`O fatorial de ${num} é igual a ${fatorial(num)}`)

//console.log(module.paths)
```

**fatorial.ts**

```typescript
export const fatorial  = (num: number): number=>{
    if(num === 0) {
      return 1
    }

    return num * fatorial (num - 1)
}
```

### Depuração de uma Aplicação NodeJS

Utilize o parâmetro **--inspect-brk**

```
node --inspect-brk dist/main.js --num=9
```

Ele começa e espera o Debugger se conectar, basta abrir o **Google Chrome**
e acessar:

```html
chrome://inspect/#devices
```

### HTTP e REST - Aulas 18 e 19

- Fala dos coneceitos básicos


### Aula 19 - Primeiro recurso REST (GET)

Objetivo: consultar a lista de usuários que está na memória.
Sem o uso do Banco de dados.

Arquivo **serve.ts**

```typescript
import * as restify from 'restify'
import {environment} from '../common/environment'
import {Router} from '../common/router'

export class Server {

  application: restify.Server

  initRoutes(routers: Router[]): Promise<any>{
    return new Promise((resolve, reject)=>{
      try{

        this.application = restify.createServer({
          name: 'meat-api',
          version: '1.0.0'
        })

        this.application.use(restify.plugins.queryParser())

        //routes
        for (let router of routers) {
          router.applyRoutes(this.application)  
        }

        this.application.listen(environment.server.port, ()=>{
           resolve(this.application)
        })

      }catch(error){
        reject(error)
      }
    })
  }

  bootstrap(routers: Router[] = []): Promise<Server>{
      return this.initRoutes(routers).then(()=> this)
  }

}

```

O nosso servidor, por enquanto, está esperando que as rotas sejam declaradas dentro do método initRoutes()

META: ao iniciar o servidor, falar para ele, quais rotas deve subir, para não deixar HARDCODE.

- No Express você consegue agrupar as rotas

