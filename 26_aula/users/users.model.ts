import * as mongoose from 'mongoose';
/** 
const users = [
  {id: '1', name:'Peter Parker', email: 'peter@marvel.com'},
  {id: '2', name:'Bruce Wayne', email: 'bruce@dc.com'}
]

export class User {
  static findAll(): Promise<any[]>{
    return Promise.resolve(users)
  }

  static findById(id: string): Promise<any>{
    return new Promise(resolve=>{
      const filtered = users.filter(user=> user.id === id)
      let user = undefined
      if(filtered.length > 0){
        user = filtered[0]
      }
      resolve(user)
    })
  }
}
*/

import * as mongoose from 'mongoose'

/** Criando o Schema do Usuario */
/**
 * Passa as informações de como será o meu documento
 */
const userSchema = new mongoose.Schema({

    name: {
      type: String
    },
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String,
      select: false
    }

})

/** Como registrar esse Schema? Usando o Model */
/** Ao passar 'User' ele ja vai usar 'users' como collection */
export const User = mongoose.model('User', userSchema)

